
const getCube = 2 ** 3;

console.log(`The cube of 2 is ${getCube}`);


const address = [258, "Washington Ave.", "NW, California", 90011]

const [addressNumber, street, state, zipCode]= address

console.log(`I live at ${addressNumber} ${street} ${state} ${zipCode}`);


const animal = {
	name: "Lolong",
	habitat: "saltwater",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, habitat, weight, measurement } = animal;

console.log(`${name} was a ${habitat} crocodile. He weighed at ${weight} with a measurement of ${measurement}.`)



const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(number)
})




class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "curly";
myDog.age = 2;
myDog.breed = "poodle";

console.log(myDog)